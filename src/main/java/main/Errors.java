package main;

/**
 * Created by Alex on 04.05.2015.
 */
public enum Errors {

    FETCHING_FAILED("Failed to fetch results",
            ""),
    CALCULATION_FALIED("Failed to calcluate results",
            ""),
    QUERY_RETURNS_NULL("Query returns no results",
            "No results for specified keyword. Please change the keyword and try again."),
    NO_KEYWORD("No keyword was specified",
            "Please specify parameter(s): keyword, username, password." + "\n" +
                    "Usage: GitHub {keyword} [username] [password]" + "\n" +
                    "keyword - is an obligatory keyword to search GitHub repositories for. \n" +
                    "username - user name (optional parameter) \n" +
                    "password - password should be specified in case Username is in place."),
    INCORRECT_USAGE("Usage is incorrect",
            "Usage: GitHub {UserName} {Password}"),
    IS_NOT_CONNECTED("Is not connected",
            "");


    public String getDescription() {
        return description;
    }

    public String getExplanation() {
        return explanation;
    }

    private String description;
    private String explanation;


    Errors(String description, String explanation) {
        this.description = description;
        this.explanation = explanation;
    }


}
