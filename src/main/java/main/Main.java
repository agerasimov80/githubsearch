package main;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsonSchema.factories.SchemaFactoryWrapper;
import com.fasterxml.jackson.databind.jsonschema.JsonSchema;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.sun.deploy.net.HttpRequest;
import github.GHRepositoryExt;
import github.GitHubAnalyzer;

import github.Pojo;
import github.PojoList;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.PagedIterator;
import org.kohsuke.github.PagedSearchIterable;

import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by Alex on 24.04.2015.
 */
public class Main {


    private static final int RESULTS_PER_PAGE = 100;

    static class MyError extends Exception {
        private String message;
        Errors errors;

        MyError(Errors errors) {

            this.message = errors.getDescription();
            this.errors = errors;
        }

        public String getMessage() {
            return message;
        }

        public Errors getErrors() {
            return errors;
        }
    }

    private static final int MAXIMUM_SEARCH = 300;
    private static final int MAXIMUM_RESULTS = 10;

    private static String keyword;
    static private String username;
    static private String password;

    public static void main(String[] args) {

    //        f1 (args);

        f2(args);
    }


    static void f2(String args[]) {

        String login;
        String password;
        String keyword;

        try {


            switch (args.length) {

                // running with parameters ...
                case 3:
                    password = args[2];
                case 2:
                    login = args[1];
                case 1:
                    keyword = args[0];
                    break;

                // running without parameters ...
                case 0:
                    throw new MyError(Errors.NO_KEYWORD);
                default:
                    throw new MyError(Errors.INCORRECT_USAGE);
            }

            ArrayList<Pojo> pojoList = new ArrayList<>();

//         URL u = new URL("https://api.github.com/search/repositories?q=tetris&page=2&per_page=100");
            String url = "https://api.github.com/search/repositories";



            int numberOfQueriesRequired = 3;
            for (int i = 0; i <  numberOfQueriesRequired; i++) {

                String query = null;
                String param1 = keyword;
                String param2 = String.valueOf(i+1);
                String param3 = String.valueOf(RESULTS_PER_PAGE);

                try {
                    query = String.format("q=%s&page=%s&per_page=%s"
                            , URLEncoder.encode(param1, String.valueOf(StandardCharsets.UTF_8))
                            , URLEncoder.encode(param2, StandardCharsets.UTF_8.name())
                            , URLEncoder.encode(param3, StandardCharsets.UTF_8.name())
                    );
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                ArrayList<Pojo> pojos = getJson(url, query);
                pojoList.addAll(pojos);
            }

            for (Pojo pojo : pojoList)
                pojo.calcWeight();

            Collections.sort(pojoList);

            int i=0;
            for ( Pojo item : pojoList) {
                System.out.println((i + 1) + ". " + item.getName() + " " + item.getWeight());
                if (i++ == MAXIMUM_RESULTS - 1) break;
            }


            //Error handler...
        } catch (MyError e) {
            System.out.println(e.getErrors().getDescription());
            System.out.println(e.getErrors().getExplanation());
            return;

        } catch (Exception e) {
            System.out.println("Unexpected error during runtime..." + e.getMessage());
            return;
        }


// GET is by default
//        try {
//            u = new URL(url + "?" + query);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        try {
//            httpURLConnection = (HttpURLConnection) u.openConnection();
//            httpURLConnection.setRequestProperty("Accept-Charset", charset);
//            inputStream = httpURLConnection.getInputStream();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        
        

/* POST
        try {
            u = new URL(url);
            httpURLConnection= (HttpURLConnection) u.openConnection();
            httpURLConnection.setDoOutput(true); // Triggers POST.
            httpURLConnection.setRequestProperty("Accept-Charset", charset);

            try (OutputStream output = httpURLConnection.getOutputStream()) {
                output.write(query.getBytes(charset));
            }

            inputStream = httpURLConnection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);

//        try {
//            JsonNode node = mapper.readTree(httpURLConnection.getInputStream()).path("items");
//
//            System.out.println("node.toString() = " + node.toString());
//
//           Iterator<JsonNode> it =  node.elements();
//            while (it.hasNext()){
//               JsonNode jsonNode =  it.next();
//               //GHRepository r =  (GHRepository) it.next();
//                System.out.println("ss");
//            }
    }

    private static ArrayList<Pojo> getJson(String url, String query) {

        ArrayList<Pojo> pojos = null;

        URL u = null;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        String json = null;

        try {
            u = new URL(url + "?" + query);
            httpURLConnection = (HttpURLConnection) u.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                sb.append(inputLine);
            in.close();

            json = sb.toString();

            PojoList list = new ObjectMapper().readValue(json, new TypeReference<PojoList>() {
            });

            pojos = new ArrayList<>(list.getList());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pojos;
    }


    static void f1(String args[]) {
        try {


            // preparing the special object to query GitHub ...
            GitHubAnalyzer gitHubAnalyzer = new GitHubAnalyzer();

            switch (args.length) {

                // running with parameters ...
                case 3:
                    gitHubAnalyzer.setPassword(args[2]);
                case 2:
                    gitHubAnalyzer.setUsername(args[1]);
                case 1:
                    gitHubAnalyzer.setKeyword(args[0]);
                    break;

                // running without parameters ...
                case 0:
                    throw new MyError(Errors.NO_KEYWORD);
                default:
                    throw new MyError(Errors.INCORRECT_USAGE);
            }

            System.out.println("Let's try to query http://github.com for results for a keyword " + args[0]);

            // connecting...
            gitHubAnalyzer.connect();
            if (!gitHubAnalyzer.isConnected()) throw new MyError(Errors.IS_NOT_CONNECTED);


            // querying ...
            if (gitHubAnalyzer.getKeyword().isEmpty()) throw new MyError(Errors.NO_KEYWORD);
            gitHubAnalyzer.queryGitHub();
            if (!gitHubAnalyzer.hasResults()) throw new MyError(Errors.QUERY_RETURNS_NULL);


            // processing results...
            PagedSearchIterable<GHRepository> ghRepositories = gitHubAnalyzer.getPagedSearchIterable();
            System.out.println("Total search results: " + ghRepositories.getTotalCount() + " repositories");


            // fetching first MAXIMUM_SEARCH results
            ArrayList<GHRepository> ghRepositoryArrayList = new ArrayList();
            PagedIterator pageIt = ghRepositories.iterator();
            fetching:
            while (pageIt.hasNext()) {
                Iterator<GHRepository> repIt = pageIt.nextPage().iterator();
                while (repIt.hasNext()) {
                    ghRepositoryArrayList.add(repIt.next());
                    if (ghRepositoryArrayList.size() >= MAXIMUM_SEARCH) break fetching;
                }
            }
            if (ghRepositoryArrayList.size() == 0) throw new MyError(Errors.FETCHING_FAILED);
            System.out.println("Fetched for analysis: " + ghRepositoryArrayList.size() + " repositories");


            // creating new Array of extended GHRepositories (GHRepositoryExt includes 'weight' property)
            ArrayList<GHRepositoryExt> ghRepositoryExArrayList = new ArrayList<>();
            for (GHRepository rep : ghRepositoryArrayList) {
                GHRepositoryExt ghRepositoryExt = new GHRepositoryExt(rep);
                ghRepositoryExArrayList.add(ghRepositoryExt);
            }
            if (ghRepositoryExArrayList.size() == 0) throw new MyError(Errors.CALCULATION_FALIED);
            ghRepositoryExArrayList.trimToSize();


            // sorting and displaying results...
            Collections.sort(ghRepositoryExArrayList);
            System.out.println("Top 10 are the following...");
            int i = 0;
            for (GHRepositoryExt item : ghRepositoryExArrayList) {
                System.out.println((i + 1) + ". " + item.getName() + " " + item.getWeight());
                if (i++ == MAXIMUM_RESULTS - 1) break;
            }

            //Error handler...
        } catch (MyError e) {
            System.out.println(e.getErrors().getDescription());
            System.out.println(e.getErrors().getExplanation());
            return;

        } catch (Exception e) {
            System.out.println("Unexpected error during runtime..." + e.getMessage());
            return;
        }
    }

}
