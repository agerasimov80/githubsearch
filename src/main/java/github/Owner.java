package github;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gerasal3 on 12.05.2015.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner {

    @JsonProperty
    private  int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
