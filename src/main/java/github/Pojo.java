package github;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.kohsuke.github.GHRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Alex on 26.04.2015.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class Pojo implements Comparable <Pojo>{

    public double getWeight() {
        return weight;
    }

    private double weight;



    @JsonProperty
    private String name;

    @JsonProperty
    private Owner owner;

    public Owner getOwner() {
        return owner;
    }
    public void setOwner(Owner owner) {
        this.owner = owner;
    }


    //    private String fullName;
    @JsonProperty
    private int forks;
    @JsonProperty
    private int watchers;
    @JsonProperty ("open_issues")
    private int openIssueCount;
    @JsonProperty
    private String language;
    @JsonProperty
    private long size;
    @JsonProperty ("pushed_at")
    private Date updatedAt;

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getWatchers() {
        return watchers;
    }

    public void setWatchers(int watchers) {
        this.watchers = watchers;
    }

    public int getOpenIssueCount() {
        return openIssueCount;
    }

    public void setOpenIssueCount(int openIssueCount) {
        this.openIssueCount = openIssueCount;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void calcWeight() {
        double weight=0.0;

        try {

            // forks  +3 for each
            if (forks != 0)
                for (int i = 0; i < forks; i++)
                    weight += 3;

            // watchers +1 for each
            if (watchers != 0)
                for (int i = 0; i < watchers; i++)
                    weight += 1;

            // open issues -1 for each
            if (openIssueCount != 0)
                for (int i = 0; i < openIssueCount; i++)
                    weight += (-1);

            // odd  -30
            if (owner.getId() != 0)
                if (owner.getId() % 2 != 0)
                    weight += -(30);

            //Java +5
            if ((language != null) && !language.isEmpty())
                if (language.equalsIgnoreCase("java"))
                    weight += 5;

            // +0.1 for each MB
            int size = 0;
            if (this.getSize() > 0) {
                long mBytes = this.getSize() % 1024;
                for (int i = 0; i < mBytes; i++)
                    weight += (.1);
            }


            // not updated more than 2 years  -20
            if (updatedAt!= null) {
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.add(Calendar.YEAR, -2);
                Date date = calendar.getTime();
                if (updatedAt.compareTo(date) == -1)
                    weight += (-20);
            }

            // rounding up
            weight = new BigDecimal(weight).setScale(2, RoundingMode.UP).doubleValue();
        }
        catch (Exception e ){
            return;
        }
        this.weight= weight;
    }

    @Override
    public int compareTo(Pojo o) {
        return Double.compare(o.getWeight(), this.getWeight());
    }


////    @Override
////    public String getName() {
////        return name;
////    }
////
////    @Override
////    public String getFullName() {
////        return fullName;
//    }

}
