package github;

import org.kohsuke.github.GHRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Alex on 26.04.2015.
 */
public class GHRepositoryExt extends GHRepository implements Comparable<GHRepositoryExt> {


    private double weight;
    private String name;
    private String fullName;

    private int forks;
    private int watchers;
    private int openIssueCount;
    private int ownerID;
    private String language;
    private long size;
    private Date updatedAt;


    public GHRepositoryExt(GHRepository repository) {
        this.forks = repository.getForks();
        this.watchers = repository.getWatchers();
        this.openIssueCount = repository.getOpenIssueCount();
        this.language = repository.getLanguage();
        this.size = repository.getSize();
        this.name = repository.getName();
        this.fullName = repository.getFullName();

        try {
            this.ownerID = repository.getOwner().getId();
        } catch (IOException e) {
            System.out.println("Error in getting User for repository");
            System.out.println("e.getMessage() = " + e.getMessage());
        }

        try {
            this.updatedAt = repository.getUpdatedAt();
        } catch (IOException e) {
            System.out.println("Error in getting Date for repository");
            System.out.println("e.getMessage() = " + e.getMessage());
        }

        this.weight = calcWeight();
    }


    public double calcWeight() {
        // forks  +3 for each
        if (forks != 0)
            for (int i = 0; i < forks; i++)
                weight += 3;

        // watchers +1 for each
        if (watchers != 0)
            for (int i = 0; i < watchers; i++)
                weight += 1;

        // open issues -1 for each
        if (openIssueCount != 0)
            for (int i = 0; i < openIssueCount; i++)
                weight += (-1);

        // odd  -30
        if (ownerID != 0)
            if (ownerID % 2 != 0)
                weight += -(30);

        //Java +5
        if ((language != null) && !language.isEmpty())
            if (language.equalsIgnoreCase("java"))
                weight += 5;

        // +0.1 for each MB
        int size = 0;
        if (this.getSize() > 0) {
            int mBytes = this.getSize() % 1024;
            for (int i = 0; i < mBytes; i++)
                weight += (.1);
        }

        // not updated more than 2 years  -20
        if (updatedAt!= null) {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.add(Calendar.YEAR, -2);
            Date date = calendar.getTime();
            if (updatedAt.compareTo(date) == -1)
                weight += (-20);
        }
        // rounding up
        weight = new BigDecimal(weight).setScale(2, RoundingMode.UP).doubleValue();


        return weight;
    }

    @Override
    public int compareTo(GHRepositoryExt o) {
        return Double.compare(o.getWeight(), this.getWeight());
    }


    public double getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

}
