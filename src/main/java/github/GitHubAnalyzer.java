package github;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import jdk.nashorn.internal.parser.JSONParser;
import org.kohsuke.github.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by Alex on 26.04.2015.
 */
public class GitHubAnalyzer {


    private static GitHub gitHub;
    private GHRepositorySearchBuilder ghRepositorySearchBuilder;
    private PagedSearchIterable pagedSearchIterable;

    private boolean isConnected;
    private boolean hasResults;

    private String username;
    private String password;
    private String keyword;

    public GitHubAnalyzer() {
    }


    public void queryGitHub() {

        ghRepositorySearchBuilder = gitHub.searchRepositories();
        ghRepositorySearchBuilder.q(keyword);

        pagedSearchIterable = ghRepositorySearchBuilder.list();

        if (pagedSearchIterable.getTotalCount() > 0)
            hasResults = true;
    }


    public void connect() {
        try {
            if (username == null || password == null)
                gitHub = GitHub.connect();
            else
                gitHub = GitHub.connectUsingPassword(username, password);

            System.out.println(gitHub.getRateLimit());
            isConnected = true;

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean hasResults() {
        return hasResults;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }


    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public PagedSearchIterable getPagedSearchIterable() {
        return pagedSearchIterable;
    }


    public static double calcWeight(int forks, int watchers, int openIssueCount, int ownerID, String language, int size, Date updatedAt, String name) {
        double weight = 0;

        System.out.println("name = " + name);


        long t1 = System.currentTimeMillis();

        // forks  +3 for each
        if (forks != 0)
            for (int i = 0; i < forks; i++)
                weight += 3;

        long t2 = System.currentTimeMillis();

//        System.out.println(t2 - t1);

        // watchers +1 for each
        if (watchers != 0)

            for (int i = 0; i < watchers; i++)
                weight += 1;


        long t3 = System.currentTimeMillis();

//        System.out.println(t3- t2);

        // open issues -1 for each
        if (openIssueCount != 0)
            for (int i = 0; i < openIssueCount; i++)
                weight += (-1);

        long t4 = System.currentTimeMillis();

        System.out.println(t4 - t3);

        // odd  -30
        if (ownerID != 0)
            if (ownerID % 2 != 0)
                weight += -(30);

        long t5 = System.currentTimeMillis();

//        System.out.println(t5- t4);

        //Java +5
        if ((language != null) && !language.isEmpty())
            if (language.equalsIgnoreCase("java"))
                weight += 5;

        long t6 = System.currentTimeMillis();

//        System.out.println(t6- t5);

        // +0.1 for each MB
        //int size = 0;
        if (size > 0) {
            int mBytes = size % 1024;
            for (long i = 0; i < mBytes; i++)
                weight += (.1);
        }

        long t7 = System.currentTimeMillis();

//        System.out.println(t7- t6);

        // not updated more than 2 years  -20
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.YEAR, -2);
        Date date = calendar.getTime();
        if (updatedAt.compareTo(date) == -1)
            weight += (-20);

        // rounding up
        weight = new BigDecimal(weight).setScale(2, RoundingMode.UP).doubleValue();

        long t8 = System.currentTimeMillis();

//        System.out.println(t8- t7);


        System.out.println();
        System.out.println("(t8-t1) = " + (t8 - t1));

        return weight;
    }


}
